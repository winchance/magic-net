﻿using Furion.DynamicApiController;
using Magic.Core;
using Magic.Core.Entity;
using Magic.Core.Service;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Magic.Web.Core;

/// <summary>
/// 系统应用服务
/// </summary>
[ApiDescriptionSettings(Name = "App", Order = 100, Tag = "系统应用服务")]
public class SysAppController : IDynamicApiController
{
    private readonly ISysAppService _service;
    public SysAppController(ISysAppService service)
    {
        _service = service;
    }

    /// <summary>
    /// 分页查询系统应用
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysApp/page")]
    public async Task<PageList<SysApp>> PageList([FromQuery] QuerySysAppPageInput input)
    {
        return await _service.PageList(input);
    }

    /// <summary>
    /// 增加系统应用
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysApp/add")]
    public async Task Add(AddSysAppParam input)
    {
        await _service.Add(input);
    }

    /// <summary>
    /// 删除系统应用
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysApp/delete")]
    public async Task Delete(Magic.Core.PrimaryKeyParam input)
    {
        await _service.Delete(input);
    }

    /// <summary>
    /// 更新系统应用
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysApp/edit")]
    public async Task Update(EditSysAppParam input)
    {
        await _service.Update(input);
    }

    /// <summary>
    /// 获取系统应用
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysApp/detail")]
    public async Task<SysApp> Get([FromQuery] Magic.Core.PrimaryKeyParam input)
    {
        return await _service.Get(input);
    }

    /// <summary>
    /// 获取系统应用列表
    /// </summary>
    /// <returns></returns>
    [HttpGet("/sysApp/list")]
    public async Task<dynamic> List()
    {
        return await _service.List();
    }

    /// <summary>
    /// 设为默认应用
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysApp/setAsDefault")]
    public async Task SetAsDefault(Magic.Core.PrimaryKeyParam input)
    {
        await _service.SetAsDefault(input);
    }

    /// <summary>
    /// 修改用户状态
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysApp/changeStatus")]
    public async Task ChangeStatus(ChangeSysAppStatusParam input)
    {
        await _service.ChangeStatus(input);
    }
}
