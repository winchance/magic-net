﻿using Furion.DynamicApiController;
using Magic.Core.Entity;
using Magic.Core;
using Magic.Core.Service;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Magic.Web.Core;

/// <summary>
/// 字典值服务
/// </summary>
[ApiDescriptionSettings(Name = "DictData", Order = 100, Tag = "字典值服务")]
public class SysDictDataController : IDynamicApiController
{
    private readonly ISysDictDataService _service;
    public SysDictDataController(ISysDictDataService service)
    {
        _service = service;
    }

    /// <summary>
    /// 分页查询字典值
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysDictData/page")]
    public async Task<PageList<SysDictData>> QueryDictDataPageList([FromQuery] QueryDictDataPageInput input)
    {
        return await _service.PageList(input);
    }

    /// <summary>
    /// 获取某个字典类型下字典值列表
    /// </summary>
    /// <returns></returns>
    [HttpGet("/sysDictData/list")]
    public async Task<List<SysDictData>> GetDictDataList([FromQuery] QueryDictDataListInput input)
    {
        return await _service.List(input);
    }

    /// <summary>
    /// 增加字典值
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysDictData/add")]
    public async Task Add(AddDictDataInput input)
    {
        await _service.Add(input);
    }

    /// <summary>
    /// 删除字典值
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysDictData/delete")]
    public async Task Delete(Magic.Core.PrimaryKeyParam input)
    {
        await _service.Delete(input);
    }

    /// <summary>
    /// 更新字典值
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysDictData/edit")]
    public async Task Update(EditDictDataInput input)
    {
        await _service.Update(input);
    }

    /// <summary>
    /// 字典值详情
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysDictData/detail")]
    public async Task<SysDictData> Get([FromQuery] Magic.Core.PrimaryKeyParam input)
    {
        return await _service.Get(input);
    }

    /// <summary>
    /// 修改字典值状态
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysDictData/changeStatus")]
    public async Task ChangeDictDataStatus(ChangeDictDataStatusInput input)
    {
        await _service.ChangeDictDataStatus(input);
    }
}
