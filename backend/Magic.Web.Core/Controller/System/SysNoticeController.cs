﻿using Furion.DynamicApiController;
using Magic.Core.Entity;
using Magic.Core;
using Magic.Core.Service;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Magic.Web.Core;

/// <summary>
/// 通知公告服务
/// </summary>
[ApiDescriptionSettings(Name = "Notice", Order = 100, Tag = "通知公告服务")]
public class SysNoticeController : IDynamicApiController
{
    private readonly ISysNoticeService _service;
    public SysNoticeController(ISysNoticeService service)
    {
        _service = service;
    }

    /// <summary>
    /// 分页查询通知公告
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysNotice/page")]
    public async Task<PageList<SysNotice>> PageList([FromQuery] QueryNoticePageInput input)
    {
        return await _service.PageList(input);
    }

    /// <summary>
    /// 增加通知公告
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysNotice/add")]
    public async Task Add(AddNoticeInput input)
    {
        await _service.Add(input);
    }

    /// <summary>
    /// 删除通知公告
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysNotice/delete")]
    public async Task Delete(PrimaryKeyParam input)
    {
        await _service.Delete(input);
    }

    /// <summary>
    /// 更新通知公告
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysNotice/edit")]
    public async Task Update(EditNoticeInput input)
    {
        await _service.Update(input);
    }

    /// <summary>
    /// 获取通知公告详情
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysNotice/detail")]
    public async Task<NoticeDetailOutput> Get([FromQuery] PrimaryKeyParam input)
    {
        return await _service.Get(input);
    }

    /// <summary>
    /// 修改通知公告状态
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysNotice/changeStatus")]
    public async Task ChangeStatus(ChangeStatusNoticeInput input)
    {
        await _service.ChangeStatus(input);
    }

    /// <summary>
    /// 获取接收的通知公告
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysNotice/received")]
    public async Task<dynamic> ReceivedNoticePageList([FromQuery] QueryNoticePageInput input)
    {
        return await _service.ReceivedNoticePageList(input);
    }

    /// <summary>
    /// 未处理消息
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysNotice/unread")]
    public async Task<dynamic> UnReadNoticeList([FromQuery] QueryNoticePageInput input)
    {
        return await _service.UnReadNoticeList(input);
    }
}
