﻿using Furion.DynamicApiController;
using Magic.Core;
using Magic.Core.Entity;
using Magic.Core.Service;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Magic.Web.Core;

/// <summary>
/// 在线用户服务
/// </summary>
[ApiDescriptionSettings(Name = "OnlineUser", Order = 100, Tag = "在线用户服务")]
public class SysOnlineUserController : IDynamicApiController
{
    private readonly ISysOnlineUserService _service;
    public SysOnlineUserController(ISysOnlineUserService service)
    {
        _service = service;
    }

    /// <summary>
    /// 获取在线用户信息
    /// </summary>
    /// <returns></returns>
    [HttpGet("/sysOnlineUser/list")]
    public async Task<dynamic> List([FromQuery] PageParamBase input) { 
        return await _service.List(input);
    }

    [HttpPost("/sysOnlineUser/forceExist")]
    [NonValidation]
    public async Task ForceExist(OnlineUser user)
    {
        await _service.ForceExist(user);
    }
}
