﻿using Furion.DynamicApiController;
using Magic.Core;
using Magic.FlowCenter.Entity;
using Magic.FlowCenter.Service;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Magic.Web.Core;

/// <summary>
/// 表单管理
/// </summary>
[ApiDescriptionSettings("FlowCenter", Name = "FlcForm", Order = 100, Tag = "表单管理")]
public class FlcFormController : IDynamicApiController
{
    private readonly IFlcFormManageService _service;

    public FlcFormController(IFlcFormManageService service)
    {
        _service = service;
    }

    /// <summary>
    /// 分页查询表单
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/flcForm/page")]
    public async Task<PageList<FlcForm>> PageList([FromQuery] QueryFlcFormPageInput input)
    {
        return await _service.PageList(input);
    }

    /// <summary>
    /// 获取表单列表
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/flcForm/list")]
    public async Task<List<FlcForm>> List([FromQuery] QueryFlcFormPageInput input)
    {
        return await _service.List(input);
    }

    /// <summary>
    /// 增加表单
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/flcForm/add")]
    public async Task Add(AddFlcFormInput input)
    {
        await _service.Add(input);
    }

    /// <summary>
    /// 删除表单
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/flcForm/delete")]
    public async Task Delete(PrimaryKeyParam input)
    {
        await _service.Delete(input);
    }

    /// <summary>
    /// 更新表单
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/flcForm/edit")]
    public async Task Update(EditFlcFormInput input)
    {
        await _service.Update(input);
    }
}
