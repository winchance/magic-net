﻿using SqlSugar;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace Magic.Core.Entity
{
    /// <summary>
    /// 文章表
    /// </summary>
    [SugarTable("sys_article")]
    [Description("文章管理表")]
    public class SysArticle : DEntityBase
    {
        /// <summary>
        /// 标题
        /// </summary>
        [Required, MaxLength(32)]
        [SugarColumn(ColumnDescription = "标题",Length =32)]
        public string Title { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        [Required]
        [SugarColumn(ColumnDescription = "内容",ColumnDataType =StaticConfig.CodeFirst_BigString)]
        public string Content { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        [SugarColumn(ColumnDescription = "类型")]
        public ArticleType Type { get; set; }

    }
}
