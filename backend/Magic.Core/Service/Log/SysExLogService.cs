﻿

using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Magic.Core.Entity;
using Microsoft.AspNetCore.Mvc;
using SqlSugar;
using System;
using System.Threading.Tasks;

namespace Magic.Core.Service;

/// <summary>
/// 异常日志服务
/// </summary>
public class SysExLogService : ISysExLogService
{
    private readonly SqlSugarRepository<SysLogEx> _sysExLogRep;  // 操作日志表仓储

    public SysExLogService(SqlSugarRepository<SysLogEx> sysExLogRep)
    {
        _sysExLogRep = sysExLogRep;
    }

    /// <summary>
    /// 分页查询异常日志
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public async Task<PageList<SysLogEx>> PageList(QueryExLogPageInput input)
    {
        var exLogs = await _sysExLogRep.AsQueryable()
                                       .WhereIF(!string.IsNullOrWhiteSpace(input.Name), u => u.Name.Contains(input.Name.Trim()))
                                       .WhereIF(!string.IsNullOrWhiteSpace(input.ClassName), u => u.ClassName == input.ClassName)
                                       .WhereIF(!string.IsNullOrWhiteSpace(input.MethodName), u => u.MethodName == input.MethodName)
                                       .WhereIF(!string.IsNullOrWhiteSpace(input.ExceptionMsg), u => u.ExceptionMsg.Contains(input.ExceptionMsg.Trim()))
                                       .WhereIF(!string.IsNullOrWhiteSpace(input.SearchBeginTime), u => u.ExceptionTime >= DateTime.Parse(input.SearchBeginTime.Trim()) &&
                                                               u.ExceptionTime <= DateTime.Parse(input.SearchEndTime.Trim()))
                                       .OrderBy(u => u.Id, OrderByType.Desc)
                                       .ToPagedListAsync(input.PageNo, input.PageSize);
        return exLogs.McPagedResult();
    }

    /// <summary>
    /// 清空异常日志
    /// </summary>
    /// <returns></returns>
    public async Task Clear()
    {
        await _sysExLogRep.DeleteAsync(m => true);
    }
}
