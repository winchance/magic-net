﻿using Furion.DependencyInjection;
using Magic.Core.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Magic.Core.Service;

public interface ISysDictDataService: ITransient
{
    Task Add(AddDictDataInput input);
    Task Add(List<AddDictDataInput> list);
    Task ChangeDictDataStatus(ChangeDictDataStatusInput input);
    Task DeleteByTypeId(long dictTypeId);
    Task Delete(Core.PrimaryKeyParam input);
    Task<SysDictData> Get(Core.PrimaryKeyParam input);
    Task<List<SysDictData>> List(QueryDictDataListInput input);
    Task<List<SysDictData>> GetDictDataListByDictTypeId(long dictTypeId);
    Task<PageList<SysDictData>> PageList(QueryDictDataPageInput input);
    Task Update(EditDictDataInput input);
    Task<List<SysDictData>> GetDictDataByCode(string code);
}
