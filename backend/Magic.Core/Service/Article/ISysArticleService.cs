﻿using Furion.DependencyInjection;
using System.Threading.Tasks;

namespace Magic.Core.Service;

public interface ISysArticleService : ITransient
{
    Task Add(AddSysArticleInput input);
    Task Delete(Core.PrimaryKeyParam input);
    Task<SysArticleDetailOutput> Get(PrimaryKeyParam input);
    Task<PageList<SysArticlePageOutput>> PageList(QuerySysArticleInput input);
    Task Update(EditSysArticleInput input);
}