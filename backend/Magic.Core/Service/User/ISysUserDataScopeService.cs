﻿using Furion.DependencyInjection;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Magic.Core.Service;

public interface ISysUserDataScopeService : ITransient
{
    Task DeleteUserDataScopeListByOrgIdList(List<long> orgIdList);
    Task DeleteUserDataScopeListByUserId(long userId);
    Task<List<long>> GetUserDataScopeIdList(long userId);
    Task GrantData(GrantUserInput input);
}
