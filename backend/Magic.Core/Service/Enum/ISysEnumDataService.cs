﻿using Furion.DependencyInjection;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Magic.Core.Service;

public interface ISysEnumDataService: ITransient
{
    dynamic GetEnumTypeList();
    Task<IEnumerable<EnumDataOutput>> GetEnumDataList( EnumDataInput input);
    Task<IEnumerable<EnumDataOutput>> GetEnumDataListByField( QueryEnumDataInput input);
}
