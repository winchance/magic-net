﻿using SqlSugar;

namespace Magic.FlowCenter.Entity;

/// <summary>
/// 自定义租户基类实体
/// </summary>
public abstract class FlcDBEntityTenant : FlcDEntityBase
{
    /// <summary>
    /// 租户id
    /// </summary>
    [SugarColumn(ColumnDescription = "租户id")]
    public virtual long? TenantId { get; set; }

}
